package com.example.primeraapp4b;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ActivityPasarParametro extends AppCompatActivity {

    EditText editDatos;
    Button buttonPasarPara;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pasar_parametro);

        buttonPasarPara=(Button)findViewById(R.id.buttonPasarPara);
        editDatos=(EditText)findViewById(R.id.editDatos);

        buttonPasarPara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityPasarParametro.this, ActivityRecibirParametro.class);
                Bundle bundle = new Bundle();
                bundle.putString("dato", editDatos.getText().toString());
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });






    }
}
