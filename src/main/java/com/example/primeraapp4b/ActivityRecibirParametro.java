package com.example.primeraapp4b;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ActivityRecibirParametro extends AppCompatActivity {
    TextView textRecibir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibir_parametro);

        textRecibir=findViewById(R.id.textRecibir);
        Bundle bundle = this.getIntent().getExtras();
        textRecibir.setText(bundle.getString("dato"));
    }
}
